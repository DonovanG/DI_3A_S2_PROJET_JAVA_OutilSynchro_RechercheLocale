package polytech.tours.di.parallel.tsp.projet;

import java.util.concurrent.Callable;

import polytech.tours.di.parallel.tsp.Instance;
import polytech.tours.di.parallel.tsp.Solution;
import polytech.tours.di.parallel.tsp.TSPCostCalculNoStatic;
import polytech.tours.di.parallel.tsp.TimeBomb;


/**
 * Implement the call method that return the best swap for one inode.
 * @author GUILLOT Donovan, AUVRAY Damien
 *
 */
public class SolutionComputation implements Callable<Solution> {

	//the current solution of the local search
	private Solution solution = null;
	//the inode's number
	private int numVille = 0;
	// the current instance that we try to resolve
	private Instance instance = null;
	//A calculator that will allow to set the objective function of a solution
	private TSPCostCalculNoStatic costCalc = new TSPCostCalculNoStatic();
	//a timer bomb that stop the program when the time expired
	private TimeBomb bomb;
	
	/**the constructor*/
	public SolutionComputation(Solution solution, Instance instance, int numVille, TimeBomb bomb){
		this.solution = solution;
		this.numVille = numVille;
		this.instance = instance;
		this.bomb = bomb;
	}
	
	/**
	 * the call method that execute our task.
	 * @return solutionEtoile : the best swap find for one inode
	 */
	@Override
	public Solution call() throws Exception {
		Solution solutionPrime = null;
        Solution solutionEtoile = solution.clone();
        
        for(int i=numVille; i<instance.getN(); i++){
            solutionPrime = solution.clone();
            solutionPrime.swap(i, numVille);            
            solutionPrime.setOF(costCalc.calcOF(instance, solutionPrime));
            
            if(solutionPrime.getOF()<solutionEtoile.getOF()){
                solutionEtoile = solutionPrime.clone();

            }
            if(bomb.exploded())
                return solutionEtoile;
        }
        return solutionEtoile;
	}
	
}

