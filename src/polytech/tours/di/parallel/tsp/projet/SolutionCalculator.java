package polytech.tours.di.parallel.tsp.projet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import polytech.tours.di.parallel.tsp.Algorithm;
import polytech.tours.di.parallel.tsp.Instance;
import polytech.tours.di.parallel.tsp.InstanceReader;
import polytech.tours.di.parallel.tsp.Solution;
import polytech.tours.di.parallel.tsp.TSPCostCalculator;
import polytech.tours.di.parallel.tsp.TimeBomb;

	/**
	 * Implements an example in which we read an instance from a file.
	 * Then we generate a random solution and do a multi-start local search on it. The parameter of the local search are in the config folder.
	 *  Finally, we print the best solution to the output console.
	 * @author GUILLOT Donovan, AUVRAY Damien
	 *
	 */
	public class SolutionCalculator implements Algorithm {

		public Solution run(Properties config) {

			//read instance
			InstanceReader ir=new InstanceReader();
			ir.buildInstance(config.getProperty("instance"));
			//get the instance 
			Instance instance=ir.getInstance();
			//read maximum CPU time
			long max_cpu=Long.valueOf(config.getProperty("maxcpu"));
			//build a random solution
			Random rnd=new Random(Long.valueOf(config.getProperty("seed")));
			
			//current solution
			Solution solution = new Solution();
			//Best solution
			Solution solutionEtoile = new Solution();
			//Neighbor best solution
			Solution solutionPrime = null;
			

			//properties
			int nbNoeuds = instance.getN();
			int nbThreads = Integer.parseInt(config.getProperty("nbThreads"));
			
			//begin
			
			//create a bomb that will go off when the time expired
			TimeBomb bomb = new TimeBomb(max_cpu, TimeUnit.SECONDS);
			
			//fill the solutions
			for(int i=0; i<instance.getN(); i++){
				solution.add(i);
				solutionEtoile.add(i);
			}

			//Instantiate the executor
			ExecutorService executor = Executors.newFixedThreadPool(nbThreads);
			
			//create tasks and the future solution's list
			List<Future<Solution>> listSolution;
			List<Callable<Solution>> tasks = new ArrayList<>();
			
			
			//shuffle the best solution to create a random solution and set it's objective function
			Collections.shuffle(solutionEtoile,rnd);
			solutionEtoile.setOF(TSPCostCalculator.calcOF(instance, solutionEtoile));

			//initialize the bomb's clock
			bomb.start();
			
			//while the bomb don't explode (time don't expired), do a new local search
			while(!bomb.exploded()){
				
				//create a random solution and set the objective function
				Collections.shuffle(solution,rnd);
				solution.setOF(TSPCostCalculator.calcOF(instance, solution));
				//initialize the neighbor solution
				solutionPrime=null;
				
				//local search
				boolean continueBoolean = true;
				while(continueBoolean )
				{
					//check the time (did the bomb exploded ?)
					if(bomb.exploded())
						return solutionEtoile;
					
					//creating tasks with the number of the inode
					for(int numNoeud=0; numNoeud<nbNoeuds; numNoeud++)
						tasks.add(new SolutionComputation(solution.clone(), instance, numNoeud, bomb));


					//Execute tasks
					try{
						listSolution = executor.invokeAll(tasks);
					}catch(InterruptedException e){
						System.out.println("ERREUR : executor");
						e.printStackTrace();
						return null;
					}
					
					//compare the best swaps' solutions, get the best
					try{
						for(Future<Solution> s : listSolution){
							if(solutionPrime==null || s.get().getOF() < solutionPrime.getOF())
								solutionPrime=s.get().clone();
						}
						if(solutionPrime.getOF()<solution.getOF())
							solution = solutionPrime.clone();
						else
							continueBoolean = false;
					}catch(InterruptedException | ExecutionException e){
						System.out.println("ERREUR : for solution : listSoluton");
						e.printStackTrace();
						return null;
					}
					
					//clear the tasks list to create new ones
					tasks.removeAll(tasks);
					//clear the neighbor solution's list to create a new one
					listSolution.removeAll(listSolution);
				}

				//check if the best neighbor solution is the best ever
				if(solutionPrime.getOF()<solutionEtoile.getOF())
					solutionEtoile=solutionPrime.clone();
			}
			//shutdown the executor
			executor.shutdown();
			return solutionEtoile;
		}
	}

