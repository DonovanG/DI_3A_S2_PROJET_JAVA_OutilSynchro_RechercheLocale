package polytech.tours.di.parallel.tsp;

import java.util.concurrent.TimeUnit;

/**
 * @author GUILLOT Donovan, AUVRAY Damien
 * make a bomb go off when the time expired.
 *
 */
public class TimeBomb {

	/** time cpu requested */
	private final long maxcpu;
	/** current time */
	private long start;
	
	/** construct a bomb with the time requested and the time unit */
	public TimeBomb(long maxcpu, TimeUnit unit){
		this.maxcpu=TimeUnit.MILLISECONDS.convert(maxcpu, unit);
	}
	
	/** synchronized the bomb's clock */
	public synchronized void start(){
		this.start=System.currentTimeMillis();
	}
	
	/** make the bomb explode */
	public boolean exploded(){
		return System.currentTimeMillis()-start>maxcpu;
	}
	
}
