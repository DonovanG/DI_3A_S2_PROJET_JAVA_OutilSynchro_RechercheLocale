package polytech.tours.di.parallel.tsp;


/**
 * Computes the cost of a TSP solution
 * @author GUILLOT Donovan, AUVRAY Damien
 * @version %I%, %G%
 *
 */
public class TSPCostCalculNoStatic {
    
    
    /**
     * Computes the objective function of a TSP tour
     * @param instance the instance data
     * @param s the solution
     * @return the objective function of solution <code>s</code>
     */
    public double calcOF(Instance instance, Solution s){
    	 double cost=0;
         for(int i=1;i<s.size();i++){
             cost=cost+instance.getDistance(s.get(i-1),s.get(i));
         }
         cost=cost+instance.getDistance(s.get(s.size()-1),s.get(0));
         return cost;
    }
   

}
